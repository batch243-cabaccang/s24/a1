const givenNumber = prompt("Give me a number so I can cube it. Hehe");

const getCube = (number) => {
  const result = Math.pow(number, 3);
  return result;
};

console.log(`The cube of ${givenNumber} is ${getCube(givenNumber)}.`);

const address = ["Somewhere", "Out There", "Over The Rainbow"];
const [a, b, c] = address;
console.log(`${a} ${b} ${c}`);

const animal = {
  name: "Elliot",
  species: "Feline",
  weight: "3kg(?)",
  measurment: "30cm(?)",
};

const { name, species, weight, measurment } = animal;
console.log(
  `Siya ay alaga ni Sir Val. Siya ay sa pamilya ng ${species}. Na merong bigat na ${weight} at merong haba/laki na ${measurment}. Siya ay walang iba kung hindi si ${name}. Hehe`
);

const numbers = [1, 2, 3, 4, 5];
numbers.forEach((number) => {
  return console.log(number);
});

const reduceNumber = () => {
  return numbers.reduce((acc, cur) => acc + cur, 0);
};

console.log(reduceNumber());

class Dog {
  constructor(name, age, breed) {
    this.name = name;
    this.age = age;
    this.breed = breed;
  }
}

const dog = new Dog("The Dog", "10 Years Old", "Yung Simple Lang");
console.log(dog);
